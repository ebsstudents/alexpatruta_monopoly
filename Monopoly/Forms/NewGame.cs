﻿using Monopoly.Entities;
using Monopoly.Forms;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Monopoly.Constants;

namespace Monopoly
{
    public partial class NewGame : Form
    {
        List<Player> players = new List<Player>();

        public NewGame()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult choice = MessageBox.Show("Are you sure you want to quit?",
                                                  "Quit Monopoly",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Warning
                                                  );
            switch (choice)
            {
                case DialogResult.None:
                    break;
                case DialogResult.Yes:
                    Application.Exit();
                    break;
                case DialogResult.No:
                    break;
                default:
                    break;
            }
        }

        private void InitializePlayerList()
        {
            players.Add(new Player { Name="", Money = 2000, Type = PlayerTypes.Boat});
            players.Add(new Player { Name = "", Money = 2000, Type = PlayerTypes.Car});
            players.Add(new Player { Name = "", Money = 2000, Type = PlayerTypes.Plane});
            players.Add(new Player { Name = "", Money = 2000, Type = PlayerTypes.Shoe});
            players.Add(new Player { Name = "", Money = 2000, Type = PlayerTypes.Tank});
        }
        private void NewGame_Load(object sender, EventArgs e)
        {
            InitializePlayerList();
            playersView.DataSource = players;
        }

        private void startGame_Click(object sender, EventArgs e)
        {
            players.RemoveAll(player => player.Money == 0 || player.Name == string.Empty);
            MonopolyGame monopolyGame = new MonopolyGame(players);
            monopolyGame.Show();
            this.Hide();
        }
    }
}
