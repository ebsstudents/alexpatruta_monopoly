﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Monopoly.Entities;

namespace Monopoly.Forms
{
    public partial class MonopolyGame : Form
    {
        Game Monopoly = new Game();

        public MonopolyGame()
        {
            InitializeComponent();
        }

        public MonopolyGame(List<Player> players)
        {
            Monopoly.playerList = players;
            InitializeComponent();
        }

        private void MonopolyGame_Load(object sender, EventArgs e)
        {
            playersDataGridView.DataSource = Monopoly.playerList;

        }

        private void MonopolyGame_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void InitializeBoard()
        {
            Monopoly.board.Add(new Cell {Name="", Cost=200, Owner= });
        }
    }
}
