﻿namespace Monopoly.Forms
{
    partial class MonopolyGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monopolySplitContainer = new System.Windows.Forms.SplitContainer();
            this.playersDataGridView = new System.Windows.Forms.DataGridView();
            this.monopolySplitContainer.Panel1.SuspendLayout();
            this.monopolySplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playersDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // monopolySplitContainer
            // 
            this.monopolySplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monopolySplitContainer.Location = new System.Drawing.Point(0, 0);
            this.monopolySplitContainer.Name = "monopolySplitContainer";
            // 
            // monopolySplitContainer.Panel1
            // 
            this.monopolySplitContainer.Panel1.Controls.Add(this.playersDataGridView);
            this.monopolySplitContainer.Size = new System.Drawing.Size(589, 276);
            this.monopolySplitContainer.SplitterDistance = 205;
            this.monopolySplitContainer.TabIndex = 0;
            // 
            // playersDataGridView
            // 
            this.playersDataGridView.AllowUserToAddRows = false;
            this.playersDataGridView.AllowUserToDeleteRows = false;
            this.playersDataGridView.AllowUserToResizeColumns = false;
            this.playersDataGridView.AllowUserToResizeRows = false;
            this.playersDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playersDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.playersDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.playersDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.playersDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.playersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playersDataGridView.GridColor = System.Drawing.Color.White;
            this.playersDataGridView.Location = new System.Drawing.Point(0, 0);
            this.playersDataGridView.Name = "playersDataGridView";
            this.playersDataGridView.ReadOnly = true;
            this.playersDataGridView.RowHeadersVisible = false;
            this.playersDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.playersDataGridView.Size = new System.Drawing.Size(202, 142);
            this.playersDataGridView.TabIndex = 0;
            // 
            // MonopolyGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(589, 276);
            this.Controls.Add(this.monopolySplitContainer);
            this.Name = "MonopolyGame";
            this.Text = "Monopoly";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MonopolyGame_FormClosed);
            this.Load += new System.EventHandler(this.MonopolyGame_Load);
            this.monopolySplitContainer.Panel1.ResumeLayout(false);
            this.monopolySplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playersDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer monopolySplitContainer;
        private System.Windows.Forms.DataGridView playersDataGridView;
    }
}