﻿namespace Monopoly
{
    partial class NewGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newGameSplitContainer = new System.Windows.Forms.SplitContainer();
            this.playersView = new System.Windows.Forms.DataGridView();
            this.quitGame = new System.Windows.Forms.Button();
            this.startGame = new System.Windows.Forms.Button();
            this.newGameSplitContainer.Panel1.SuspendLayout();
            this.newGameSplitContainer.Panel2.SuspendLayout();
            this.newGameSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playersView)).BeginInit();
            this.SuspendLayout();
            // 
            // newGameSplitContainer
            // 
            this.newGameSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newGameSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.newGameSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.newGameSplitContainer.Name = "newGameSplitContainer";
            this.newGameSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // newGameSplitContainer.Panel1
            // 
            this.newGameSplitContainer.Panel1.Controls.Add(this.playersView);
            // 
            // newGameSplitContainer.Panel2
            // 
            this.newGameSplitContainer.Panel2.BackColor = System.Drawing.Color.White;
            this.newGameSplitContainer.Panel2.Controls.Add(this.quitGame);
            this.newGameSplitContainer.Panel2.Controls.Add(this.startGame);
            this.newGameSplitContainer.Size = new System.Drawing.Size(463, 269);
            this.newGameSplitContainer.SplitterDistance = 209;
            this.newGameSplitContainer.TabIndex = 0;
            // 
            // playersView
            // 
            this.playersView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playersView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.playersView.BackgroundColor = System.Drawing.Color.White;
            this.playersView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.playersView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playersView.GridColor = System.Drawing.Color.White;
            this.playersView.Location = new System.Drawing.Point(0, 0);
            this.playersView.Name = "playersView";
            this.playersView.RowHeadersVisible = false;
            this.playersView.Size = new System.Drawing.Size(463, 209);
            this.playersView.TabIndex = 0;
            // 
            // quitGame
            // 
            this.quitGame.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.quitGame.Location = new System.Drawing.Point(0, 0);
            this.quitGame.Name = "quitGame";
            this.quitGame.Size = new System.Drawing.Size(112, 56);
            this.quitGame.TabIndex = 1;
            this.quitGame.Text = "QUIT";
            this.quitGame.UseVisualStyleBackColor = true;
            this.quitGame.Click += new System.EventHandler(this.button1_Click);
            // 
            // startGame
            // 
            this.startGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.startGame.Location = new System.Drawing.Point(118, 0);
            this.startGame.Name = "startGame";
            this.startGame.Size = new System.Drawing.Size(345, 56);
            this.startGame.TabIndex = 0;
            this.startGame.Text = "START";
            this.startGame.UseVisualStyleBackColor = true;
            this.startGame.Click += new System.EventHandler(this.startGame_Click);
            // 
            // NewGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 269);
            this.Controls.Add(this.newGameSplitContainer);
            this.MinimumSize = new System.Drawing.Size(274, 308);
            this.Name = "NewGame";
            this.Text = "New Game";
            this.Load += new System.EventHandler(this.NewGame_Load);
            this.newGameSplitContainer.Panel1.ResumeLayout(false);
            this.newGameSplitContainer.Panel2.ResumeLayout(false);
            this.newGameSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playersView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer newGameSplitContainer;
        private System.Windows.Forms.DataGridView playersView;
        private System.Windows.Forms.Button startGame;
        private System.Windows.Forms.Button quitGame;
    }
}

