﻿using Monopoly.Constants;

namespace Monopoly.Entities
{
    public class Player
    {
        #region private members
            private string name;
            private int money;
            private PlayerTypes type;
        #endregion

        #region public members
            public string Name
            {
                get { return name; }
                set { name = value; }
            }
            
            public int Money
            {
                get { return money; }
                set { money = value; }
            }

            public PlayerTypes Type
            {
                get { return type; }
                set { type = value; }
            }
        #endregion
    }
}
