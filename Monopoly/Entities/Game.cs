﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monopoly.Entities
{
    class Game
    {
        public List<Player> playerList = new List<Player>();
        public List<Cell> board = new List<Cell>();
        public Dice dice;
    }
}
