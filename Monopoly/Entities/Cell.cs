﻿using Monopoly.Constants;

namespace Monopoly.Entities
{
    public class Cell
    {
        string name;
        int cost;
        int rent;
        int position;
        Constants.PlayerTypes owner;
        int x, y;

        public int Y
        {
            get { return this.y; }
            set { this.Y = value; }
        }
        public int Cost
        {
            get
            {
                return cost;
            }

            set
            {
                cost = value;
            }
        }
        public int X
        {
            get
            {
                return x;
            }

            set
            {
                x = value;
            }
        }
        public PlayerTypes Owner
        {
            get
            {
                return owner;
            }

            set
            {
                owner = value;
            }
        }
        public int Position
        {
            get
            {
                return position;
            }

            set
            {
                position = value;
            }
        }
        public int Rent
        {
            get
            {
                return rent;
            }

            set
            {
                rent = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
    }
}